﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WebSubsystem.h"
#include "HttpManager.h"

FString UWebSubsystem::GetErrorMessage(const FString& RawErrorString)
{
	return {};
}

EWebSubsystemErrorCode UWebSubsystem::GetErrorCode(const FString& RawErrorString)
{
	return {};
}

void UWebSubsystem::Flush()
{
	FHttpModule::Get().GetHttpManager().Flush(EHttpFlushReason::FullFlush);
}

void UWebSubsystem::ProcessRequest(FHttpRequestRef HttpRequest)
{
	if (HttpRequest->GetURL().IsEmpty())
	{
		return;
	}
	if (LogEnabled)
	{
		UE_LOG(LogTemp, Display, TEXT("Request processing started: %s"), *HttpRequest->GetURL());
	}
	if (!HttpRequest->ProcessRequest())
	{
		LogError(FString::Printf(TEXT("Request processing failed: %s"), *HttpRequest->GetURL()));
		RequestError.Broadcast(HttpRequest->GetURL(), {});
	}
}

bool UWebSubsystem::Success(FHttpResponsePtr Response, bool WasSuccessful)
{
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());
	TSharedPtr<FJsonObject> JsonObject;

	if (!FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		LogError("Json deserialization error");
		RequestError.Broadcast(Response->GetURL(), Response->GetContentAsString());
		return false;
	}
	if (!WasSuccessful || !JsonObject.IsValid() || JsonObject->HasField("error"))
	{
		LogError(Response->GetContentAsString());
		RequestError.Broadcast(Response->GetURL(), Response->GetContentAsString());
		return false;
	}
	LogResponse(Response);
	return true;
}

void UWebSubsystem::LogResponse(FHttpResponsePtr Response)
{
	if (LogEnabled)
	{
		UE_LOG(LogTemp, Display, TEXT("Request URL: %s"), *Response->GetURL());
		UE_LOG(LogTemp, Display, TEXT("%s"), *Response->GetContentAsString());
	}
}

void UWebSubsystem::LogError(const FString& ErrorText)
{
	UE_LOG(LogTemp, Error, TEXT("%s"), *ErrorText);
}

void UWebSubsystem::SetOptional(TSharedPtr<FJsonObject> RequestBody, const TOptional<FString>& Param,
	const FString& ParamName)
{
	if (!Param.IsSet()) return;
	RequestBody->SetStringField(ParamName, Param.GetValue());
}

void UWebSubsystem::SetOptional(TSharedPtr<FJsonObject> RequestBody, const TOptional<TArray<float>>& Param,
	const FString& ParamName)
{
	if (!Param.IsSet()) return;

	TArray<TSharedPtr<FJsonValue>> ArrayParam;
	for (float ParamValue : Param.GetValue())
	{
		ArrayParam.Add(MakeShareable(new FJsonValueNumber(ParamValue)));
	}
	RequestBody->SetArrayField(ParamName, ArrayParam);
}
