﻿#pragma once

#include "CoreMinimal.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnRequestComplited, const FString&, const FString&);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnRequestProgress, const FString&, const FString&);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnRequestError, const FString&, const FString&);

UENUM(BlueprintType)
enum class EWebSubsystemErrorCode : uint8
{
	ERROR = 0,
	CANCELLED = 1,
	UNKNOWN = 2
};