﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "HttpModule.h"
#include "JsonObjectConverter.h"
#include "WebSubsystemTypes.h"
#include "Interfaces/IHttpResponse.h"
#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"
#include "WebSubsystem.generated.h"

UCLASS()
class SIMPLEWEB_API UWebSubsystem : public UObject
{
	GENERATED_BODY()

public:
	FOnRequestError& OnRequestError() { return RequestError; }
	void SetLogEnabled(bool bEnabled) { LogEnabled = bEnabled; }
	FString GetErrorMessage(const FString& RawErrorString);
	EWebSubsystemErrorCode GetErrorCode(const FString& RawErrorString);
	virtual void Flush();
	
protected:
	FOnRequestError RequestError;
	bool LogEnabled {false};

	virtual void ProcessRequest(FHttpRequestRef HttpRequest);
	virtual bool Success(FHttpResponsePtr Response, bool WasSuccessful);
	virtual void LogResponse(FHttpResponsePtr Response);
	virtual void LogError(const FString& ErrorText);

	template <typename OutStructType>
	FString SerializeRequest(const OutStructType& OutStruct) const
	{
		TSharedPtr<FJsonObject> RequestBody = FJsonObjectConverter::UStructToJsonObject<OutStructType>(OutStruct);
		
		FString OutputString;
		TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&OutputString);
		FJsonSerializer::Serialize(RequestBody.ToSharedRef(), Writer);

		return OutputString;
	}

	template <typename OutStructType>
	FHttpRequestRef MakeRequest(const OutStructType& OutStruct, const FString& URL, const FString& Metod) const
	{
		auto HttpRequest = CreateNewHttpRequest();
		HttpRequest->SetHeader("Content-Type", "application/json");
		HttpRequest->SetURL(URL);
		HttpRequest->SetVerb(Metod);
		HttpRequest->SetContentAsString(SerializeRequest(OutStruct));
		return HttpRequest;
	}

	template <typename OutStructType>
	FHttpRequestRef MakeRequest(const FString& URL, const FString& Metod) const
	{
		auto HttpRequest = CreateNewHttpRequest();
		HttpRequest->SetHeader("Content-Type", "application/json");
		HttpRequest->SetURL(URL);
		HttpRequest->SetVerb(Metod);
		return HttpRequest;
	}

	template <typename OutStructType>
	bool ParseJsonToStruct(const FString& Data, OutStructType* OutStruct)
	{
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Data);
		TSharedPtr<FJsonObject> JsonObject;
		if (!FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			return false;
		}
		FJsonObjectConverter::JsonObjectToUStruct(JsonObject.ToSharedRef(), OutStruct);
		return true;
	}

	template <typename ParsedResponseType, typename DelegateType>
	void HandleResponse(FHttpResponsePtr Response, bool bWasSuccessful, DelegateType& Delegate)
	{
		if (!Success(Response, bWasSuccessful)) return;
		ParsedResponseType ParsedResponse;
		if (ParseJsonToStruct(Response->GetContentAsString(), &ParsedResponse))
		{
			Delegate.Broadcast(ParsedResponse);
		}
		else
		{
			LogError("Json parse error");
			RequestError.Broadcast(Response->GetURL(), Response->GetContentAsString());
		}
	}

	template <typename ParsedResponseType>
	ParsedResponseType HandleResponse(FHttpResponsePtr Response, bool bWasSuccessful)
	{
		if (!Success(Response, bWasSuccessful)) return ParsedResponseType{};
		ParsedResponseType ParsedResponse;
		if (ParseJsonToStruct(Response->GetContentAsString(), &ParsedResponse))
		{
			return ParsedResponse;
		}
		LogError("Json parse error");
		RequestError.Broadcast(Response->GetURL(), Response->GetContentAsString());
		return ParsedResponseType{};
	}

	virtual TSharedRef<IHttpRequest, ESPMode::ThreadSafe> CreateNewHttpRequest() const
	{
		return FHttpModule::Get().CreateRequest();
	}

	void SetOptional(TSharedPtr<FJsonObject> RequestBody, const TOptional<FString>& Param, const FString& ParamName);
	void SetOptional(TSharedPtr<FJsonObject> RequestBody, const TOptional<TArray<float>>& Param, const FString& ParamName);

	template <typename NumberType>
	void SetOptional(TSharedPtr<FJsonObject> RequestBody, const TOptional<NumberType>& Param, const FString& ParamName)
	{
		if (!Param.IsSet()) return;
		RequestBody->SetNumberField(ParamName, Param.GetValue());
	}
};
