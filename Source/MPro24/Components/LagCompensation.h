﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LagCompensation.generated.h"

class ACharacterBase;

USTRUCT()
struct FBoxInformation
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Location;

	UPROPERTY()
	FRotator Rotation;

	UPROPERTY()
	FVector BoxExtent;
};

USTRUCT()
struct FFramePackage
{
	GENERATED_BODY()

	UPROPERTY()
	float Time;

	UPROPERTY()
	TMap<FName, FBoxInformation> HitBoxInfo;

	UPROPERTY()
	ACharacterBase* Character;
};

USTRUCT()
struct FServerRewindResult
{
	GENERATED_BODY()

	UPROPERTY()
	bool HitConfirmed;

	UPROPERTY()
	bool HeadShot;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MPRO24_API ULagCompensation : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ULagCompensation();
	friend class ACharacterBase;
	friend class AMPro24PlayerController;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
							   FActorComponentTickFunction* ThisTickFunction) override;

	void ShowFramePackage(const FFramePackage& Package);

	// Hitscan
	FServerRewindResult ServerSideRewind(
		ACharacterBase* HitCharacter, 
		const FVector_NetQuantize& TraceStart, 
		const FVector_NetQuantize& HitLocation, 
		float HitTime);

	// Projectile/
	FServerRewindResult ProjectileServerSideRewind(
		ACharacterBase* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime
	);

	UFUNCTION(Server, Reliable)
	void ServerScoreRequest(
		ACharacterBase* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize& HitLocation,
		float HitTime
	);

	UFUNCTION(Server, Reliable)
	void ProjectileServerScoreRequest(
		ACharacterBase* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime
	);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	void SaveFramePackage(FFramePackage& Package);
	FFramePackage InterpolateBetweenFrames(const FFramePackage& OldPackage, const FFramePackage& NewPackage, float HitTime);
	void CacheBoxPositions(ACharacterBase* HitCharacter, FFramePackage& OutPackage);
	void MoveBoxes(ACharacterBase* HitCharacter, const FFramePackage& OutPackage);
	void ResetHitBoxes(ACharacterBase* HitCharacter, const FFramePackage& OutPackage);
	void EnableCharacterMeshCollision(ACharacterBase* HitCharacter, ECollisionEnabled::Type CollisionEnabled);
	void SaveFramePackage();
	FFramePackage GetFrameToCheck(ACharacterBase* HitCharacter, float HitTime);

	// Hitscan/
	FServerRewindResult ConfirmHit(
		const FFramePackage& Package,
		ACharacterBase* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize& HitLocation);

	// Projectile/
	FServerRewindResult ProjectileConfirmHit(
		const FFramePackage& Package,
		ACharacterBase* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime
	);

private:
	UPROPERTY()
	ACharacterBase* OwnerCharacter;

	UPROPERTY()
	AMPro24PlayerController* OwnerController;

	TDoubleLinkedList<FFramePackage> FrameHistory;

	UPROPERTY(EditAnywhere)
	float MaxRecordTime = 5.f;
};
