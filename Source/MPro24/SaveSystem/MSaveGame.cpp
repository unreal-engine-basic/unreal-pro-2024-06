﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MSaveGame.h"

#include "GameFramework/PlayerState.h"

FPlayerSaveData* UMSaveGame::GetPlayerSaveData(APlayerState* PlayerState)
{
	if (PlayerState == nullptr) return nullptr;

	if (PlayerState->GetWorld()->IsPlayInEditor())
	{
		if (SavedPlayers.IsValidIndex(0))
		{
			return &SavedPlayers[0];
		}
		return nullptr;
	}

	FString PlayerID = PlayerState->GetUniqueId().GetUniqueNetId()->ToString();
	return SavedPlayers.FindByPredicate([&](const FPlayerSaveData& Data) { return Data.PlayerID == PlayerID; });
}
