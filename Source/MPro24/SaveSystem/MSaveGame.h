﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MSaveGame.generated.h"

USTRUCT()
struct FActorSaveData
{
	GENERATED_BODY()
	
	UPROPERTY()
	FName ActorName;

	UPROPERTY()
	FTransform Transform;

	UPROPERTY()
	TArray<uint8> ByteData;
};

USTRUCT()
struct FPlayerSaveData
{
	GENERATED_BODY()

	FPlayerSaveData()
	{
		Score = 0.f;
		Defeats = 0;
		PlayerLocation = FVector::ZeroVector;
		PlayerRotation = FRotator::ZeroRotator;
	}

	UPROPERTY()
	FString PlayerID;

	UPROPERTY()
	float Score;

	UPROPERTY()
	int32 Defeats;

	UPROPERTY()
	FVector PlayerLocation;

	UPROPERTY()
	FRotator PlayerRotation;
};

UCLASS()
class MPRO24_API UMSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TArray<FPlayerSaveData> SavedPlayers;

	UPROPERTY()
	TArray<FActorSaveData> SavedActors;

	FPlayerSaveData* GetPlayerSaveData(APlayerState* PlayerState);
};
