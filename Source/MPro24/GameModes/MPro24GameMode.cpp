// Copyright Epic Games, Inc. All Rights Reserved.

#include "MPro24GameMode.h"

#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "MPro24/MPro24Character.h"
#include "MPro24/GameStates/FreeFoeAllGameState.h"
#include "MPro24/GameStates/Mpro24PlayerState.h"
#include "MPro24/Player/MPro24PlayerController.h"
#include "UObject/ConstructorHelpers.h"

namespace MatchState
{
	const FName Cooldown = FName("Cooldown");
}

AMPro24GameMode::AMPro24GameMode()
{
	bDelayedStart = true;
}

void AMPro24GameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MatchState == MatchState::WaitingToStart)
	{
		CountdownTime = WarmupTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			StartMatch();
		}
	}
	else if (MatchState == MatchState::InProgress)
	{
		CountdownTime = WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			SetMatchState(MatchState::Cooldown);
		}
	}
	else if (MatchState == MatchState::Cooldown)
	{
		CountdownTime = CooldownTime + WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			RestartGame();
		}
	}
}

void AMPro24GameMode::PlayerEliminated(class AMPro24Character* ElimmedCharacter,
	class AMPro24PlayerController* VictimController, AMPro24PlayerController* AttackerController)
{
	if (AttackerController == nullptr || AttackerController->PlayerState == nullptr) return;
	if (VictimController == nullptr || VictimController->PlayerState == nullptr) return;
	AMpro24PlayerState* AttackerPlayerState = AttackerController ? Cast<AMpro24PlayerState>(AttackerController->PlayerState) : nullptr;
	AMpro24PlayerState* VictimPlayerState = VictimController ? Cast<AMpro24PlayerState>(VictimController->PlayerState) : nullptr;

	AFreeFoeAllGameState* FreeFoeAllGameState = GetGameState<AFreeFoeAllGameState>();

	if (AttackerPlayerState && AttackerPlayerState != VictimPlayerState && FreeFoeAllGameState)
	{
		TArray<AMpro24PlayerState*> PlayersCurrentlyInTheLead;
		for (auto LeadPlayer : FreeFoeAllGameState->TopScoringPlayers)
		{
			PlayersCurrentlyInTheLead.Add(LeadPlayer);
		}

		AttackerPlayerState->AddToScore(1.f);
		FreeFoeAllGameState->UpdateTopScore(AttackerPlayerState);
		if (FreeFoeAllGameState->TopScoringPlayers.Contains(AttackerPlayerState))
		{
			AMPro24Character* Leader = Cast<AMPro24Character>(AttackerPlayerState->GetPawn());
			if (Leader)
			{
				// MulticastGainedTheLead();
			}
		}

		for (int32 i = 0; i < PlayersCurrentlyInTheLead.Num(); i++)
		{
			if (!FreeFoeAllGameState->TopScoringPlayers.Contains(PlayersCurrentlyInTheLead[i]))
			{
				AMPro24Character* Loser = Cast<AMPro24Character>(PlayersCurrentlyInTheLead[i]->GetPawn());
				if (Loser)
				{
					// MulticastLostTheLead();
				}
			}
		}
	}
	if (VictimPlayerState)
	{
		VictimPlayerState->AddToDefeats(1);
	}

	if (ElimmedCharacter)
	{
		// ElimmedCharacter->Elim(false);
	}

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		AMPro24PlayerController* PC = Cast<AMPro24PlayerController>(*It);
		if (PC && AttackerPlayerState && VictimPlayerState)
		{
			PC->BroadcastElim(AttackerPlayerState, VictimPlayerState);
		}
	}
}

void AMPro24GameMode::RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController)
{
	if (ElimmedCharacter)
	{
		ElimmedCharacter->Reset();
		ElimmedCharacter->Destroy();
	}
	if (ElimmedController)
	{
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), PlayerStarts);
		int32 Selection = FMath::RandRange(0, PlayerStarts.Num() - 1);
		RestartPlayerAtPlayerStart(ElimmedController, PlayerStarts[Selection]);
	}
}

void AMPro24GameMode::PlayerLeftGame(class AMpro24PlayerState* PlayerLeaving)
{
	if (PlayerLeaving == nullptr) return;
	AFreeFoeAllGameState* FreeFoeAllGameState = GetGameState<AFreeFoeAllGameState>();
	if (FreeFoeAllGameState && FreeFoeAllGameState->TopScoringPlayers.Contains(PlayerLeaving))
	{
		FreeFoeAllGameState->TopScoringPlayers.Remove(PlayerLeaving);
	}
	AMPro24Character* CharacterLeaving = Cast<AMPro24Character>(PlayerLeaving->GetPawn());
	if (CharacterLeaving)
	{
		//CharacterLeaving->Elim(true);
	}
}

float AMPro24GameMode::CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage)
{
	return BaseDamage;
}

void AMPro24GameMode::BeginPlay()
{
	Super::BeginPlay();
	LevelStartingTime = GetWorld()->GetTimeSeconds();
}

void AMPro24GameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		AMPro24PlayerController* PC = Cast<AMPro24PlayerController>(*It);
		if (PC)
		{
			PC->OnMatchStateSet(MatchState);
		}
	}
}
