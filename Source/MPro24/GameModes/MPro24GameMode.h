// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MPro24GameMode.generated.h"

namespace MatchState
{
	// Match duration has been reached. Display winner and begin cooldown timer.
	extern MPRO24_API const FName Cooldown;
}

UCLASS()
class AMPro24GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AMPro24GameMode();
	virtual void Tick(float DeltaTime) override;
	virtual void PlayerEliminated(class AMPro24Character* ElimmedCharacter, class AMPro24PlayerController* VictimController, AMPro24PlayerController* AttackerController);
	virtual void RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController);
	void PlayerLeftGame(class AMpro24PlayerState* PlayerLeaving);
	virtual float CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage);
	UPROPERTY(EditDefaultsOnly)
	float WarmupTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	float MatchTime = 120.f;

	UPROPERTY(EditDefaultsOnly)
	float CooldownTime = 10.f;

	float LevelStartingTime = 0.f;
	
	FORCEINLINE float GetCountdownTime() const { return CountdownTime; }
protected:
	virtual void BeginPlay() override;
	virtual void OnMatchStateSet() override;

private:
	float CountdownTime = 0.f;
};



