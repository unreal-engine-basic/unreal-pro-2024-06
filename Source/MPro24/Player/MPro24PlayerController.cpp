﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MPro24PlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Image.h"
#include "MPro24/MPro24Character.h"
#include "MPro24/GameModes/MPro24GameMode.h"
#include "MPro24/GameStates/Mpro24PlayerState.h"
#include "MPro24/Types/Announcement.h"
#include "MPro24/UI/ReturnToMainMenu.h"

void AMPro24PlayerController::SetHUDHealth(float Health, float MaxHealth)
{
}

void AMPro24PlayerController::SetHUDShield(float Shield, float MaxShield)
{
}

void AMPro24PlayerController::SetHUDScore(float Score)
{
}

void AMPro24PlayerController::SetHUDDefeats(int32 Defeats)
{
}

void AMPro24PlayerController::SetHUDWeaponAmmo(int32 Ammo)
{
}

void AMPro24PlayerController::SetHUDCarriedAmmo(int32 Ammo)
{
}

void AMPro24PlayerController::SetHUDMatchCountdown(float CountdownTime)
{
}

void AMPro24PlayerController::SetHUDAnnouncementCountdown(float CountdownTime)
{
}

void AMPro24PlayerController::SetHUDGrenades(int32 Grenades)
{
}

void AMPro24PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AMPro24Character* HumanCharacter = Cast<AMPro24Character>(InPawn);
	if (HumanCharacter)
	{
		//SetHUDHealth(HumanCharacter->GetHealth(), HumanCharacter->GetMaxHealth());
	}
}

void AMPro24PlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetHUDTime();
	CheckTimeSync(DeltaTime);
	CheckPing(DeltaTime);
}

void AMPro24PlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMPro24PlayerController, MatchState);
}

float AMPro24PlayerController::GetServerTime()
{
	if (HasAuthority()) return GetWorld()->GetTimeSeconds();
	else return GetWorld()->GetTimeSeconds() + ClientServerDelta;
}

void AMPro24PlayerController::ReceivedPlayer()
{
	Super::ReceivedPlayer();
	if (IsLocalController())
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
	}
}

void AMPro24PlayerController::OnMatchStateSet(FName State)
{
	MatchState = State;

	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void AMPro24PlayerController::HandleMatchHasStarted()
{
}

void AMPro24PlayerController::HandleCooldown()
{
	AMPro24Character* BlasterCharacter = Cast<AMPro24Character>(GetPawn());
	if (BlasterCharacter)
	{
	}
}

void AMPro24PlayerController::BroadcastElim(APlayerState* Attacker, APlayerState* Victim)
{
	ClientElimAnnouncement(Attacker, Victim);
}

void AMPro24PlayerController::BeginPlay()
{
	Super::BeginPlay();
	ServerCheckMatchState();
}

void AMPro24PlayerController::SetHUDTime()
{
	float TimeLeft = 0.f;
	if (MatchState == MatchState::WaitingToStart) TimeLeft = WarmupTime - GetServerTime() + LevelStartingTime;
	else if (MatchState == MatchState::InProgress) TimeLeft = WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	else if (MatchState == MatchState::Cooldown) TimeLeft = CooldownTime + WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	uint32 SecondsLeft = FMath::CeilToInt(TimeLeft);
	
	if (HasAuthority())
	{
		if (MPro24GameMode == nullptr)
		{
			MPro24GameMode = Cast<AMPro24GameMode>(UGameplayStatics::GetGameMode(this));
			LevelStartingTime = MPro24GameMode->LevelStartingTime;
		}
		MPro24GameMode = MPro24GameMode == nullptr ? Cast<AMPro24GameMode>(UGameplayStatics::GetGameMode(this)) : MPro24GameMode;
		if (MPro24GameMode)
		{
			SecondsLeft = FMath::CeilToInt(MPro24GameMode->GetCountdownTime() + LevelStartingTime);
		}
	}

	if (CountdownInt != SecondsLeft)
	{
		if (MatchState == MatchState::WaitingToStart || MatchState == MatchState::Cooldown)
		{
			SetHUDAnnouncementCountdown(TimeLeft);
		}
		if (MatchState == MatchState::InProgress)
		{
			SetHUDMatchCountdown(TimeLeft);
		}
	}

	CountdownInt = SecondsLeft;
}

void AMPro24PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (InputComponent == nullptr) return;

	InputComponent->BindAction("Quit", IE_Pressed, this, &AMPro24PlayerController::ShowReturnToMainMenu);
}

void AMPro24PlayerController::ServerRequestServerTime_Implementation(float TimeOfClientRequest)
{
	float ServerTimeOfReceipt = GetWorld()->GetTimeSeconds();
	ClientReportServerTime(TimeOfClientRequest, ServerTimeOfReceipt);
}

void AMPro24PlayerController::ClientReportServerTime_Implementation(float TimeOfClientRequest,
	float TimeServerReceivedClientRequest)
{
	float RoundTripTime = GetWorld()->GetTimeSeconds() - TimeOfClientRequest;
	SingleTripTime = 0.5f * RoundTripTime;
	float CurrentServerTime = TimeServerReceivedClientRequest + SingleTripTime;
	ClientServerDelta = CurrentServerTime - GetWorld()->GetTimeSeconds();
}

void AMPro24PlayerController::CheckTimeSync(float DeltaTime)
{
	TimeSyncRunningTime += DeltaTime;
	if (IsLocalController() && TimeSyncRunningTime > TimeSyncFrequency)
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
		TimeSyncRunningTime = 0.f;
	}
}

void AMPro24PlayerController::ServerCheckMatchState_Implementation()
{
	if (AMPro24GameMode* GameMode = Cast<AMPro24GameMode>(UGameplayStatics::GetGameMode(this)))
	{
		WarmupTime = GameMode->WarmupTime;
		MatchTime = GameMode->MatchTime;
		CooldownTime = GameMode->CooldownTime;
		LevelStartingTime = GameMode->LevelStartingTime;
		MatchState = GameMode->GetMatchState();
		ClientJoinMidgame(MatchState, WarmupTime, MatchTime, CooldownTime, LevelStartingTime);
	}
}

void AMPro24PlayerController::ClientJoinMidgame_Implementation(FName StateOfMatch, float Warmup, float Match,
	float Cooldown, float StartingTime)
{
	WarmupTime = Warmup;
	MatchTime = Match;
	CooldownTime = Cooldown;
	LevelStartingTime = StartingTime;
	MatchState = StateOfMatch;
	OnMatchStateSet(MatchState);
}

void AMPro24PlayerController::HighPingWarning()
{
}

void AMPro24PlayerController::StopHighPingWarning()
{
}

void AMPro24PlayerController::CheckPing(float DeltaTime)
{
	if (HasAuthority()) return;
	HighPingRunningTime += DeltaTime;
	if (HighPingRunningTime > CheckPingFrequency)
	{
		TObjectPtr<APlayerState> Self = GetPlayerState<APlayerState>();
		PlayerState = PlayerState == nullptr ? Self : PlayerState;
		if (PlayerState)
		{
			if (PlayerState->GetCompressedPing() * 4 > HighPingThreshold) // ping is compressed; it's actually ping / 4
			{
				HighPingWarning();
				PingAnimationRunningTime = 0.f;
				ServerReportPingStatus(true);
			}
			else
			{
				ServerReportPingStatus(false);
			}
		}
		HighPingRunningTime = 0.f;
	}
	bool bHighPingAnimationPlaying = true;
	if (bHighPingAnimationPlaying)
	{
		PingAnimationRunningTime += DeltaTime;
		if (PingAnimationRunningTime > HighPingDuration)
		{
			StopHighPingWarning();
		}
	}
}

void AMPro24PlayerController::ShowReturnToMainMenu()
{
	if (ReturnToMainMenuWidget == nullptr) return;
	if (ReturnToMainMenu == nullptr)
	{
		ReturnToMainMenu = CreateWidget<UReturnToMainMenu>(this, ReturnToMainMenuWidget);
	}
	if (ReturnToMainMenu)
	{
		bReturnToMainMenuOpen = !bReturnToMainMenuOpen;
		if (bReturnToMainMenuOpen)
		{
			ReturnToMainMenu->MenuSetup();
		}
		else
		{
			ReturnToMainMenu->MenuTearDown();
		}
	}
}

FString AMPro24PlayerController::GetInfoText(const TArray<AMpro24PlayerState*>& Players)
{
	AMpro24PlayerState* Mpro24PlayerState = GetPlayerState<AMpro24PlayerState>();
	if (Mpro24PlayerState == nullptr) return FString();
	FString InfoTextString;
	if (Players.Num() == 0)
	{
		InfoTextString = Announcement::ThereIsNoWinner;
	}
	else if (Players.Num() == 1 && Players[0] == Mpro24PlayerState)
	{
		InfoTextString = Announcement::YouAreTheWinner;
	}
	else if (Players.Num() == 1)
	{
		InfoTextString = FString::Printf(TEXT("Winner: \n%s"), *Players[0]->GetPlayerName());
	}
	else if (Players.Num() > 1)
	{
		InfoTextString = Announcement::PlayersTiedForTheWin;
		InfoTextString.Append(FString("\n"));
		for (auto TiedPlayer : Players)
		{
			InfoTextString.Append(FString::Printf(TEXT("%s\n"), *TiedPlayer->GetPlayerName()));
		}
	}

	return InfoTextString;
}

void AMPro24PlayerController::ClientElimAnnouncement_Implementation(APlayerState* Attacker, APlayerState* Victim)
{
	APlayerState* Self = GetPlayerState<APlayerState>();
	if (Attacker && Victim && Self)
	{
		if (Attacker == Self && Victim != Self)
		{
			return;
		}
		if (Victim == Self && Attacker != Self)
		{
			return;
		}
		if (Attacker == Victim && Attacker == Self)
		{
			return;
		}
		if (Attacker == Victim && Attacker != Self)
		{
			return;
		}
	}
}

void AMPro24PlayerController::OnRep_MatchState()
{
	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void AMPro24PlayerController::ServerReportPingStatus_Implementation(bool bHighPing)
{
	HighPingDelegate.Broadcast(bHighPing);
}
