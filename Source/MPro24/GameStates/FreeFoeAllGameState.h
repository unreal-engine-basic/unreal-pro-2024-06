﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "FreeFoeAllGameState.generated.h"


UCLASS()
class MPRO24_API AFreeFoeAllGameState : public AGameState
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void UpdateTopScore(class AMpro24PlayerState* ScoringPLayer);
	
	UPROPERTY(Replicated)
	TArray<class AMpro24PlayerState*> TopScoringPlayers;

private:
	float TopScore = 0;
};
