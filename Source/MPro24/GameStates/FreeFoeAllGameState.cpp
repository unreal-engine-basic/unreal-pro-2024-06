﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FreeFoeAllGameState.h"

#include "Mpro24PlayerState.h"
#include "Net/UnrealNetwork.h"

void AFreeFoeAllGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFreeFoeAllGameState, TopScoringPlayers);
}

void AFreeFoeAllGameState::UpdateTopScore(class AMpro24PlayerState* ScoringPLayer)
{
	if (TopScoringPlayers.Num() == 0)
	{
		TopScoringPlayers.Add(ScoringPLayer);
		TopScore = ScoringPLayer->GetScore();
	}
	else if (TopScore == ScoringPLayer->GetScore())
	{
		TopScoringPlayers.AddUnique(ScoringPLayer);
	}
	else if (ScoringPLayer->GetScore() > TopScore)
	{
		TopScoringPlayers.Empty();
		TopScoringPlayers.AddUnique(ScoringPLayer);
		TopScore = ScoringPLayer->GetScore();
	}
}
