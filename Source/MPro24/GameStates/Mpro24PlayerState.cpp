﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Mpro24PlayerState.h"

#include "MPro24/MPro24Character.h"
#include "MPro24/Player/MPro24PlayerController.h"
#include "MPro24/SaveSystem/MSaveGame.h"
#include "Net/UnrealNetwork.h"

#include UE_INLINE_GENERATED_CPP_BY_NAME(Mpro24PlayerState)

void AMpro24PlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMpro24PlayerState, Defeats);
}

void AMpro24PlayerState::OnRep_Score()
{
	Super::OnRep_Score();

	Character = Character == nullptr ? Cast<AMPro24Character>(GetPawn()) : Character;
	if (Character)
	{
		Controller = Controller == nullptr ? Cast<AMPro24PlayerController>(Character->Controller) : Controller;
		if (Controller)
		{
			Controller->SetHUDScore(GetScore());
		}
	}
}

void AMpro24PlayerState::OnRep_Defeats()
{
	Character = Character == nullptr ? Cast<AMPro24Character>(GetPawn()) : Character;
	if (Character)
	{
		Controller = Controller == nullptr ? Cast<AMPro24PlayerController>(Character->Controller) : Controller;
		if (Controller)
		{
			Controller->SetHUDDefeats(Defeats);
		}
	}
}

void AMpro24PlayerState::AddToScore(float ScoreAmount)
{
	SetScore(GetScore() + ScoreAmount);
	Character = Character == nullptr ? Cast<AMPro24Character>(GetPawn()) : Character;
	if (Character)
	{
		Controller = Controller == nullptr ? Cast<AMPro24PlayerController>(Character->Controller) : Controller;
		if (Controller)
		{
			Controller->SetHUDScore(GetScore());
		}
	}
}

void AMpro24PlayerState::AddToDefeats(int32 DefeatsAmount)
{
	Defeats += DefeatsAmount;
	Character = Character == nullptr ? Cast<AMPro24Character>(GetPawn()) : Character;
	if (Character)
	{
		Controller = Controller == nullptr ? Cast<AMPro24PlayerController>(Character->Controller) : Controller;
		if (Controller)
		{
			Controller->SetHUDDefeats(Defeats);
		}
	}
}

void AMpro24PlayerState::LoadPlayerState_Implementation(UMSaveGame* SaveObject)
{
	if (SaveObject)
	{
		if (const FPlayerSaveData* FoundData = SaveObject->GetPlayerSaveData(this))
		{
			AddToScore(FoundData->Score);
			AddToDefeats(FoundData->Defeats);
		}
	}
}

void AMpro24PlayerState::SavePlayerState_Implementation(UMSaveGame* SaveObject)
{
	if (SaveObject)
	{
		// Gather all relevant data for player
		FPlayerSaveData SaveData;
		SaveData.Score = GetScore();
		SaveData.Defeats = Defeats;
		SaveData.PlayerID = GetUniqueId().GetUniqueNetId()->ToString();

		// May not be alive while we save
		if (const APawn* MyPawn = GetPawn())
		{
			SaveData.PlayerLocation = MyPawn->GetActorLocation();
			SaveData.PlayerRotation = MyPawn->GetActorRotation();
		}
		
		SaveObject->SavedPlayers.Add(SaveData);
	}
}
