// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MPro24 : ModuleRules
{
	public MPro24(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", 
			"EnhancedInput", "AIModule", "GameplayTasks", "DeveloperSettings" });
        PrivateDependencyModuleNames.AddRange(new string[] { "Weapons", "Health", "Inventory", "AnimGraphRuntime", "MultiplayerSessions" });
        DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");

    }
}
