// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupItem.h"
#include "HealthComponent.h"

APickupItem::APickupItem()
{
	PrimaryActorTick.bCanEverTick = true;

}

void APickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool APickupItem::Interact_Implementation(AActor* User)
{
	if (!User)
		return false;

	return Pickup(User);
}

bool APickupItem::Pickup(AActor* User)
{
	switch (ItemType)
	{
		case EItemType::Health:
		{
			return HealthPickup(User);
		}
		case EItemType::Weapon:
		{
			//return EquipWeapon();
			break;
		}
	}

	return false;
}

bool APickupItem::HealthPickup(AActor* User)
{
	auto HealthComponent = User->FindComponentByClass<UHealthComponent>();
	if (!HealthComponent)
		return false;

	if (HealthComponent->Heal(HealValue))
	{
		Destroy();
		return true;
	}
	else
	{
		return false;
	}
}

