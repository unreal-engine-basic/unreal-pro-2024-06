﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIControllerBase.generated.h"

UCLASS()
class MPRO24_API AAIControllerBase : public AAIController
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAIControllerBase();

	virtual void OnPossess(APawn* InPawn) override;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BTComponent;

	UPROPERTY(Transient)
	class UBlackboardComponent* BBComponent;

	UPROPERTY(BlueprintReadWrite)
	class ACharacterBase* Agent = nullptr;

	UPROPERTY(BlueprintReadWrite)
	float DetectionLevel = 0.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
