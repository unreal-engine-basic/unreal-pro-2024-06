﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "../CommonDefinitions.h"
#include "CharacterBase.generated.h"

USTRUCT(BlueprintType)
struct FAnimValues
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite, Category = Movement)
	bool bIsCrouching = false;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	bool bIsInCombat = false;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	bool bIsShooting = false;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	bool bADS = false;

	UPROPERTY(BlueprintReadWrite, Category = Movement)
	bool bIsSitting = false;

};


UCLASS(config=Game)
class ACharacterBase : public ACharacter
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere)
	class ULagCompensation* LagCompensation;
	
public:
	ACharacterBase();

	virtual void PostInitializeComponents() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	class USkeletalMeshComponent* ConstantWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	TArray<class AWeaponBase*> WeaponsBase;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	EFaction Faction;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	ECombatRole CombatRole;

	UPROPERTY(BlueprintReadOnly, Category="AI")
	bool Dead = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="AI")
	float Health = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="AI")
	float BaseDamage = 0.01f;

	UPROPERTY(BlueprintReadOnly, Category="AI")
	class AAIControllerBase* ControllerRef = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="AI")
	class UBehaviorTree* BehaviorTreeAsset = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="AI")
	class ASmartObject* SmartObject = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Animation")
	FAnimValues AnimValues;

	UPROPERTY()
	TMap<FName, class UBoxComponent*> HitCollisionBoxes;

	UFUNCTION(BlueprintCallable)
	void PlayMontage(UAnimMontage* Montage, float Rate);

	bool IsHostile(ACharacterBase* Agent);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateWidgetRef();

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateWidgetVis(bool Newbool);

	FORCEINLINE ULagCompensation* GetLagCompensation() const { return LagCompensation; }

protected:
	virtual void BeginPlay() override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
	void MoveForward(float Val);
	UFUNCTION(BlueprintCallable)
	void MoveRight(float Val);
	UFUNCTION(BlueprintCallable)
	void LookUp(float Val);
	UFUNCTION(BlueprintCallable)
	void Turn(float Val);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float SlowWalkSpeed = 94.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 375.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 110.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float CrouchedWalkSpeed = 100.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Combat")
	UAnimMontage* DeathMontage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Combat")
	UAnimMontage* HitReactMontage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Combat")
	UAnimMontage* FireWeaponMontage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Combat")
	UAnimMontage* ReloadMontage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Combat")
	UAnimMontage* SwapMontage;
};
